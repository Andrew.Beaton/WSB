# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 21:13:02 2021

@author: Andrew
"""

import pandas as pd


from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split


datapath = "C:\\Users\\Andrew\\Documents\\DSets\\reddit_wsb_2.csv"
wsb=pd.read_csv(datapath)
wsb.score.describe()
#Cutting exceptions 
wsb= wsb[(wsb["score"]  <= 25000)]  
#Counting the body field
wsb['body_count'] = wsb['body'].str.split().str.len()
wsb['title_count'] = wsb['title'].str.split().str.len()

wsb = wsb.dropna(axis=0)



wsb_features = ['body_count','title_count','comms_num']

x = wsb[wsb_features]

y  = wsb.score

#spliting 
train_X, val_X, train_y, val_y = train_test_split(x, y, random_state = 0)


forest_model = RandomForestRegressor(random_state=1)
forest_model.fit(train_X, train_y)
melb_preds = forest_model.predict(val_X)
print(mean_absolute_error(val_y, melb_preds))
print wsb.score.describe()