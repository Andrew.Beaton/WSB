# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 12:06:17 2021

@author: Andrew
"""

from sklearn.model_selection import train_test_split
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder
import pandas as pd 
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error


numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']


datapath = "C:\\Users\\Andrew\\Documents\\DSets\\melb_data.csv"
melb=pd.read_csv(datapath)
#spliting 
melb.dropna(axis=0, subset=["Price"], inplace=True)

#Pulls price data and drop from the main dataset
y = melb.Price
melb.drop(["Price"],axis=1, inplace =True)

x=melb

                      

#Splits data into training and val sets 
train_X, val_X, train_y, val_y = train_test_split(x, y,train_size=0.8, test_size=0.2, random_state = 0)

s = (train_X.dtypes == 'object')
object_cols = list(s[s].index)



#numerical_cols = train_X.select_dtypes(include = numerics)
numerical_cols = [cname for cname in train_X.columns if 
                train_X[cname].dtype in ['int64', 'float64']]

X_numeric = train_X[numerical_cols].copy()


cat_cols = [cname for cname in train_X.columns if
                    train_X[cname].nunique() < 10 and 
                    train_X[cname].dtype == "object"]


#cat_cols = train_X.select_dtypes(include = 'object')

# Numerical data
numerical_transformer = SimpleImputer(strategy = "constant")



#Catagorical Data
categorical_transformer =  Pipeline(steps  = [
        ("imputer", SimpleImputer(strategy = "most_frequent")),
        ("onehot" , OneHotEncoder(handle_unknown ="ignore"))
        ])



#Combine pre processing
preprocessor = ColumnTransformer(
        transformers  = [
                ("num",  numerical_transformer, numerical_cols),
                ("cat", categorical_transformer, cat_cols)
                ])

#Gathering into a pipeline
forest_model = RandomForestRegressor(random_state=0)

prep_pipeline = Pipeline(
        steps=[("preprocessor",preprocessor),
               ("forest_model",forest_model)
        ])

prep_pipeline.fit(train_X, train_y)
preds = prep_pipeline.predict(val_X)
score = mean_absolute_error(val_y, preds)
print('MAE:', score)
