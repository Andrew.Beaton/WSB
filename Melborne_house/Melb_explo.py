# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 13:21:13 2021

@author: Andrew
"""


import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.impute import SimpleImputer


def score_dataset(max_leaf_nodes, train_X, val_X, train_y, val_y):
    model = RandomForestRegressor(max_leaf_nodes=max_leaf_nodes, random_state=0)
    model.fit(train_X, train_y)
    preds_val = model.predict(val_X)
    mae = mean_absolute_error(val_y, preds_val)
    return(mae)
    
def sum_null(dataset):
    return dataset.isnull().sum()    
    
def cols_with_null(dataset):
    miss_col_names =[]
    for col in train_X.columns:
        if train_X[col].isnull().any():
            miss_col_names.append(col)
    return miss_col_names


datapath = "C:\\Users\\Andrew\\Documents\\DSets\\melb_data.csv"
melb=pd.read_csv(datapath)


melb.dropna(axis=0, subset=["Price"], inplace=True)
y = melb.Price
melb.drop(["Price"],axis=1, inplace =True)


X =melb.select_dtypes(exclude = ['object'])

X_test = melb.select_dtypes(exclude=['object'])



#spliting 
train_X, val_X, train_y, val_y = train_test_split(X, y, random_state = 0)




#Looking for missing data 
#miss_count_by_col = train_X.isnull().sum()
miss_count_by_col = sum_null(train_X)
print  miss_count_by_col[miss_count_by_col != 0]

miss_col_names = cols_with_null(train_X)
                      
reduced_train_X = train_X.drop(miss_col_names, axis =1)
reduced_val_X = val_X.drop(miss_col_names, axis = 1 )


print("MAE (Drop columns with missing values):")
print(score_dataset(1000,reduced_train_X, reduced_val_X, train_y, val_y))



####
# imputation
####

my_imputer = SimpleImputer()
imp_train_X = pd.DataFrame(my_imputer.fit_transform(train_X))
imp_val_X = pd.DataFrame(my_imputer.transform(val_X))

#Adding back column names

imp_train_X.columns = train_X.columns
imp_val_X.columns =  val_X.columns

print("MAE from Approach 2 (Imputation):")
print(score_dataset(1000,imp_train_X, imp_val_X, train_y, val_y))
"""
cols_with_missing  = [col for col in train_X.columns
                      if train_X[col].isnull().any()]
# Drop columns in training and validation data
reduced_train_X = train_X.drop(cols_with_missing, axis=1)
reduced_val_X = val_X.drop(cols_with_missing, axis=1)

print("MAE from Approach 1 (Drop columns with missing values):")
print(score_dataset(reduced_train_X, reduced_val_X, train_y, val_y))

"""