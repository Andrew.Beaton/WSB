# -*- coding: utf-8 -*-
"""
Created on Mon Feb 15 16:38:07 2021

@author: Andrew
"""
from sklearn.model_selection import train_test_split
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder
import pandas as pd 
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error


numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']


datapath = "C:\\Users\\Andrew\\Documents\\DSets\\melb_data.csv"
melb=pd.read_csv(datapath)
#spliting 
melb.dropna(axis=0, subset=["Price"], inplace=True)
y = melb.Price
melb.drop(["Price"],axis=1, inplace =True)

x=melb

                      


train_X, val_X, train_y, val_y = train_test_split(x, y, random_state = 0)

s = (train_X.dtypes == 'object')
object_cols = list(s[s].index)

numerical_cols = train_X.select_dtypes(include = numerics)
cat_cols = train_X.select_dtypes(include = 'object')

# Numerical data
numerical_transformer = SimpleImputer(strategy = "constant")



#Catagorical Data
categorical_transformer =  Pipeline(steps  = [
        ("imputer", SimpleImputer(strategy = "most_frequent")),
        ("onehot" , OneHotEncoder(handle_unknown ="ignore"))
        ])



#Combine pre processing
preprocessor = ColumnTransformer(
        transformers  = [
                ("num",  numerical_transformer, numerical_cols),
                ("cat", categorical_transformer, cat_cols)
                ])

#Gathering into a pipeline
forest_model = RandomForestRegressor(random_state=1)

prep_pipeline = Pipeline(
        steps=[("preprocessor",preprocessor),
               ("forest_model",forest_model)
        ])

prep_pipeline.fit(train_X, train_y)
preds = prep_pipeline.predict(val_X)
score = mean_absolute_error(val_y, preds)
print'MAE:', score