# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 13:27:38 2021

@author: Andrew
"""

from sklearn.model_selection import train_test_split
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder
import pandas as pd 
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import cross_val_score
import numpy as np
numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']


datapath = "C:\\Users\\Andrew\\Documents\\DSets\\melb_data.csv"
melb=pd.read_csv(datapath)
#spliting 
melb.dropna(axis=0, subset=["Price"], inplace=True)
y = melb.Price
melb.drop(["Price"],axis=1, inplace =True)

x=melb

                      


train_X, val_X, train_y, val_y = train_test_split(x, y,train_size=0.8, test_size=0.2, random_state = 0)

s = (train_X.dtypes == 'object')
object_cols = list(s[s].index)

#numerical_cols = train_X.select_dtypes(include = numerics)
numerical_cols = [cname for cname in train_X.columns if 
                train_X[cname].dtype in ['int64', 'float64']]




cat_cols = [cname for cname in train_X.columns if
                    train_X[cname].nunique() < 10 and 
                    train_X[cname].dtype == "object"]


#cat_cols = train_X.select_dtypes(include = 'object')

# Numerical data
numerical_transformer = SimpleImputer(strategy = "constant")



#Catagorical Data
categorical_transformer =  Pipeline(steps  = [
        ("imputer", SimpleImputer(strategy = "most_frequent")),
        ("onehot" , OneHotEncoder(handle_unknown ="ignore"))
        ])



#Combine pre processing
preprocessor = ColumnTransformer(
        transformers  = [
                ("num",  numerical_transformer, numerical_cols),
                ("cat", categorical_transformer, cat_cols)
                ])

#Gathering into a pipeline
forest_model = RandomForestRegressor(random_state=0)

cross_pipeline = Pipeline(
        steps=[("preprocessor",preprocessor),
               ("forest_model",forest_model)
        ])

var_est_pipeline = Pipeline(
        steps=[("preprocessor",preprocessor),
               ("forest_model",RandomForestRegressor(n_estimators=50,random_state=0))
        ]) 

X_vals = train_X[numerical_cols].copy()
#Y_vals = train_y[cat_cols].copy()
"""
scores = -1 * cross_val_score(cross_pipeline, train_X, train_y,
                              cv=5,
                              scoring='neg_mean_absolute_error')
"""
#print("MAE scores:\n", scores)
def c_val_av(pipe,X,Y,folds):
    scores = -1 * cross_val_score(pipe, X,Y,
                              cv=folds,
                              scoring='neg_mean_absolute_error')
    
    return np.average(scores)

def pipe_depth(est):
    var_pipe = Pipeline(
        steps = [("preprocessor",preprocessor),
                 ("forest_model",RandomForestRegressor(n_estimators=est,random_state=0))
                 ])
    return var_pipe

averages = c_val_av(var_est_pipeline,train_X,train_y,3)
testDepth = np.arange(50,400,50)

depthlist = []
for depth in testDepth:
    score = c_val_av(pipe_depth(depth),train_X,train_y,3)
    depthlist.append(score)
    