# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 21:13:02 2021

@author: Andrew
"""

import pandas as pd


from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split


datapath = "C:\\Users\\Andrew\\Documents\\DSets\\reddit_wsb_2.csv"
wsb=pd.read_csv(datapath)


def get_mae(max_leaf_nodes, train_X, val_X, train_y, val_y):
    model = DecisionTreeRegressor(max_leaf_nodes=max_leaf_nodes, random_state=0)
    model.fit(train_X, train_y)
    preds_val = model.predict(val_X)
    mae = mean_absolute_error(val_y, preds_val)
    return(mae)
    
    
#Counting the body field
wsb['body_count'] = wsb['body'].str.split().str.len()
wsb['title_count'] = wsb['title'].str.split().str.len()

wsb = wsb.dropna(axis=0)



wsb_features = ['body_count','title_count','comms_num']

x = wsb[wsb_features]

y  = wsb.score

#spliting 
train_X, val_X, train_y, val_y = train_test_split(x, y, random_state = 0)

# compare MAE with differing values of max_leaf_nodes
for max_leaf_nodes in [5, 50, 500, 5000]:
    my_mae = get_mae(max_leaf_nodes, train_X, val_X, train_y, val_y)
    print("Max leaf nodes: %d  \t\t Mean Absolute Error:  %d" %(max_leaf_nodes, my_mae))
    
    #Best depth is 50 leaves