# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 20:27:30 2021

@author: Andrew
"""

import pandas as pd


from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import train_test_split

datapath = "C:\\Users\\Andrew\\Documents\\DSets\\reddit_wsb_2.csv"
wsb=pd.read_csv(datapath)

#Counting the body field
wsb['body_count'] = wsb['body'].str.split().str.len()
wsb['title_count'] = wsb['title'].str.split().str.len()

wsb = wsb.dropna(axis=0)


y  = wsb.score


wsb_features = ['body_count','title_count','comms_num']

x = wsb[wsb_features]
wsb_model = DecisionTreeRegressor(random_state=1)

#spliting 
train_X, val_X, train_y, val_y = train_test_split(x, y, random_state = 0)
# Fit model
wsb_model.fit(train_X, train_y)
print "next five =="
print x.head()

print("The predictions are")
print(wsb_model.predict(x.head()))

#mean absolute error 
predicted_score = wsb_model.predict(val_X)
mae = mean_absolute_error(val_y,predicted_score)

print mae/wsb["score"].mean()